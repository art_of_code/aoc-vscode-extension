# How to use the ArtofCode vscode extension

## Installation

After you've downloaded the file above you can install it using two ways:

### First way (by using the command line)

``` bash
cd "Place where you've downloaded the extension"
code --install-extension "artofcode-beta-$VERSION.vsix"
```

### Second way (by using interface)

![how to install](./.images/doc-aoc-install.gif)

## First use

You should be connected for using AoC, so if you are using the extension for the first time, you will be asked to connect to your account.

![first use](./.images/doc-aoc-first-use.gif)

## Analysing a file

### Start an analyse

Right click on the file want to analyse and select __Let AoC analyse this file__

![analyse button](./.images/doc-aoc-analyse-this-file.png)

### View reported errors / warnings

When an error or a warning is detected by AoC your code will be marked at the position where the error occurred.

![view errors 1](./.images/doc-aoc-view-error-1.gif)

More information can be show in the problem window of your IDE (maj+shift+m) or by hovering with your mouse the code where the error occured.

If you want more details about an error, you must use the button __Help me__ (see below) it will redirect you to an advanced documentation about that error.
![view errors 2](./.images/doc-aoc-view-error-2.png)
![view errors 3](./.images/doc-aoc-view-error-3.png)

## Log out

You can log out use the logout command: Open the command prompt __(ctrl+shift+p)__ and enter `Logout your AoC account`