/* eslint-disable @typescript-eslint/class-name-casing */
import * as vscode from 'vscode';
import command from "./command";
import aoc_api from "../Services/api";

export class logout extends command {
  
  constructor(private _api: aoc_api) {
    super('artofcode.logout');
  }

  protected async handler() {
    await this._api.logout();
    vscode.window.showInformationMessage(`You are now logged out`, ...[ 'OK' ]);
  }

}