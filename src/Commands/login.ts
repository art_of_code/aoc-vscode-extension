/* eslint-disable @typescript-eslint/class-name-casing */
import * as vscode from 'vscode';
import command from "./command";
import aoc_api from "../Services/api";
import axios from 'axios';
const login_page = require('../misc/login-page.html').default;

let current_panel: vscode.WebviewPanel | undefined = undefined;
let current_promise: Promise<boolean> | undefined = undefined;

export class login extends command {
  
  constructor(private _context: vscode.ExtensionContext, private _api: aoc_api) {
    super('artofcode.login');
  }

  private _on_login_success() {
    const measurement_id = 'G-C3X120Q6YK';
    const api_secret = 'FIBQjPyzTL2T71CaG_EPvg';
    const client_id = '66466798-f0c9-44a6-b138-f8154f8034d8';

    axios.post(`https://www.google-analytics.com/mp/collect?measurement_id=${measurement_id}&api_secret=${api_secret}`,
      {
        client_id: client_id,
        events: [{
          name: 'login-ext',
        }]
      }
    );
  }

  protected async handler() {
    let   ready = false;
    
    if (!!current_panel) {
      const column_to_show_in = vscode.window.activeTextEditor
        ? vscode.window.activeTextEditor.viewColumn
        : undefined;
      
      current_panel.reveal(column_to_show_in);
      return current_promise;
    }
    const panel = vscode.window.createWebviewPanel(
      'aoc_login_page',
      'ArtofCode login page',
      vscode.ViewColumn.One,
      {
        enableScripts: true
      }
    );
    current_panel = panel;

    return current_promise = new Promise((resolve) => {
      let resolved = false;
      panel.onDidDispose(() => { 
        current_panel = undefined;
        current_promise = undefined;
        if (!resolved) { resolve(false); }
      });

      panel.webview.onDidReceiveMessage(
        async (message) => {
          switch (message.code) {
            case 'on-ready':
              ready = true;
              break;
            case 'on-submit':
              try {
                const res = await this._api.login(<string>message.username, <string>message.password);
                panel.webview.postMessage({
                  code: 'on-connection-finished',
                  status: 'success'
                });
                vscode.window.showInformationMessage(`Hello ${res.attributes.name}. You are now logged in`, ...[ 'OK' ]);
                resolved = true;
                this._on_login_success();
                resolve(true);
                panel.dispose();
              } catch (e) {
                vscode.window.showErrorMessage(`Connection to AoC failed (${e.message})`);
                panel.webview.postMessage({
                  code: 'on-connection-finished',
                  status: 'fail',
                  error: e
                });
              }
              break;
            default:
              console.error('Got unknow code from login webview');
              break;
          }
        },
        undefined,
        this._context.subscriptions
      );
      panel.webview.html = login_page as any;
    });
  }

}