/* eslint-disable @typescript-eslint/class-name-casing */
import * as vscode from 'vscode';
import * as _ from 'lodash';
import * as fs from 'fs';
import command from "./command";
import aoc_api from "../Services/api";
import files from "../Services/files";
import axios from 'axios';
import { md_factory } from "../Services/MdFactory";

const diag_collection_id = 'aoc_vscode_extension';
const diag_collection = vscode.languages.createDiagnosticCollection(diag_collection_id);
const diag_decorator = vscode.window.createTextEditorDecorationType({
  overviewRulerColor: "#6495ED",
  overviewRulerLane: vscode.OverviewRulerLane.Right,
  backgroundColor: 'transparent',
});

export class single_file_check extends command {
  
  constructor(private _api: aoc_api) {
    super('artofcode.runFileCheck');
  }

  protected async handler(uri: vscode.Uri) {
    if (!uri) {
      vscode.window.showInformationMessage('You must right click on a file to start an analyse');
      return;
    }
    try {
      await this._api.get_session();
    } catch {
      const v = await vscode.window.showInformationMessage('You must be connected to use AoC. Connect now ?', ...[ 'yes', 'no' ]);
      if (v === 'yes') {
        if (!await vscode.commands.executeCommand('artofcode.login')) {
          vscode.window.showInformationMessage('The analysis was canceled because the login was aborted.', ...[ 'OK' ]);
          return;
        }
      } else {
        return;
      }
    }
    vscode.window.withProgress({
      location: vscode.ProgressLocation.Notification,
      title: "Art Of Code is processing your file",
      cancellable: true
    }, (_progress, token) => {
      let abort_token = this._api.create_cancel_token();

      token.onCancellationRequested(() => {
        abort_token.abort();
      });

      return files.getFileContent(uri).then(async (buffer: Buffer | any) => {
        let doc = await vscode.window.showTextDocument(uri);
        try {
          var data = await this._api.runCheck(uri.path, buffer, abort_token);
          const measurement_id = 'G-C3X120Q6YK';
          const api_secret = 'FIBQjPyzTL2T71CaG_EPvg';
          const client_id = '66466798-f0c9-44a6-b138-f8154f8034d8';

          axios.post(`https://www.google-analytics.com/mp/collect?measurement_id=${measurement_id}&api_secret=${api_secret}`,
            {
              client_id: client_id,
              events: [{
                name: 'start-analyse',
              }]
            }
          );
        } catch (e) {
          if (!e.message) {
            e.message = "Unkown Error from server";
          }
          vscode.window.showErrorMessage(e.message, ...[ "OK", "SHOW MORE" ]).then(res => {
            if (res === "SHOW MORE") {
              const report_file = require('os').tmpdir() + '/aoc_server_error_report.json';

              fs.writeFileSync(report_file, JSON.stringify(e.infos, null, 2));

              let uri = vscode.Uri.parse(report_file);
              vscode.workspace.openTextDocument(uri).then((a: vscode.TextDocument) => {
                vscode.window.showTextDocument(a, 1, false).then(_e => {});
            }, (error: any) => {
                console.error(error);
                debugger;
            });
            }
          });
          return;
        }
  
        let diag_decorations: vscode.DecorationOptions[] = [];
        let ds: vscode.Diagnostic[] = [];
  
        diag_collection.clear();
  
        await Promise.all(_.map(data, async (value) => {
          const r = new vscode.Range(
            new vscode.Position(value.start.line, value.start.column),
            new vscode.Position(value.end.line, value.end.column)
          );
          let d: vscode.Diagnostic = {
            range: r,
            message: value.message,
            severity: value.type === "INFO" ? vscode.DiagnosticSeverity.Warning : vscode.DiagnosticSeverity.Error,
            source: 'AoC',
            code: value.title,
          };
          ds.push(d);

          try {
            var doc_ids: string[] = (await this._api.find_doc(value.title)).map((d: any) => d.id);
          } catch {
            var doc_ids: string[] = [];
          }

          const getLink = () => {
            switch (vscode.workspace.getConfiguration().get('ArtOfCode.servers')) {
              case 'prod': return 'https://backoffice.artofcode.eu/DocumentationPage/';
              default: return 'https://dev.backoffice.artofcode.eu/DocumentationPage/';
            }
          };
  
          const decorations = diag_decorations;
          decorations.push({
            hoverMessage: md_factory.makeJsError({
              title: value.title,
              message: value.message,
              link: (doc_ids.length > 0)
                      ? getLink() + doc_ids[0]
                      : undefined
            }),
            range: new vscode.Range(
              new vscode.Position(value.start.line, value.start.column),
              new vscode.Position(value.end.line, value.end.column)
            )
          });
          
        }));
        diag_collection.set(uri, ds);
  
        doc.setDecorations(diag_decorator, diag_decorations);
      }).catch((err: NodeJS.ErrnoException) => vscode.window.showErrorMessage(err.message));
    });
  }

}