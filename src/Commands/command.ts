/* eslint-disable @typescript-eslint/class-name-casing */

export default class command {

  protected handler(...args: any[]): any { console.log('not implemented'); };

  constructor(
    protected command_name: string,
  ) { }

  public get_args() { return [ this.command_name, this.handler, this ]; }

}