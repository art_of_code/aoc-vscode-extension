/* eslint-disable @typescript-eslint/class-name-casing */
import * as vscode from "vscode";
import * as _ from "lodash";
import * as fs from 'fs';

const workspace = vscode.workspace;

class files {

  constructor() {}

  private _findEntryPoint() {

  }

  getCurrentWorkspace(): vscode.Uri | null {
    let name = workspace.name;

    if (!!name) {
      const ws = _(workspace.workspaceFolders).find((e: vscode.WorkspaceFolder) => (!!e) ? e.name === name : false);

      return (!!ws) ? ws.uri : null;
    }

    return null;
  }

  getFileContent = async (uri: vscode.Uri): Promise<Buffer> => {
    return new Promise((resolve, reject) => {
      fs.readFile(uri.fsPath, (err, buf) => {
        if (err) { reject(err); }
        resolve(buf);
      });
    });
  };

};

const files_ = new files();
export default files_;