/* eslint-disable @typescript-eslint/class-name-casing */
/*
** EPITECH PROJECT, 2018
** eip_forward_vscode_extension
** File description:
** api.ts
*/

'use strict';

import { workspace, ExtensionContext } from "vscode";
import * as path from "path";
import * as FormData from 'form-data';
import * as _ from 'lodash';
import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js';
import { URL } from 'url';
import cognito_storage from '../misc/cognito_storage';
import axios from "axios";

const rand = (min: number, max: number) => Math.floor(Math.random() * Math.floor(max - min)) + min;

const get_server_uri = () => {
  const servers = {
    prod: {
      server: "https://api.artofcode.eu/prod/",
      cognito: {
        pool_data: {    
          UserPoolId : 'eu-west-2_zWWRt7OUA',
          ClientId : '3v38ln516auoov0m28rhvvb729'
        },
        pool_region: 'eu-west-2'
      }
    },
    dev: {
      server: "https://api.artofcode.eu/dev/",
      cognito: {
        pool_data: {    
          UserPoolId : 'eu-west-2_szbED3sX3',
          ClientId : '7lhstj289aojhlta04sr7kutdu'
        },
        pool_region: 'eu-west-2'
      }
    },
    local: {
      server: "http://localhost:3000",
      cognito: {
        pool_data: {    
          UserPoolId : 'eu-west-2_szbED3sX3',
          ClientId : '7lhstj289aojhlta04sr7kutdu'
        },
        pool_region: 'eu-west-2'
      }
    }
  };

  switch (workspace.getConfiguration().get('ArtOfCode.servers')) {
    case "prod": return servers.prod;
    case "dev": return servers.dev;
    case "local": return servers.local;
    default: return servers.prod;
  }
};

let apiBaseUrl = `${get_server_uri().server}/`;

export interface JSErrorSign {
  title: string,
  type: string,
  message: string,
  link: string,
  start: {
    line: number,
    column: number,
  },
  end: {
    line: number,
    column: number,
  }
}

class api {

  private get _pool_data() { return get_server_uri().cognito.pool_data; }
  private get _pool_region() { return get_server_uri().cognito.pool_region; }
  private _user_pool: AmazonCognitoIdentity.CognitoUserPool;
  private _cognito_user: AmazonCognitoIdentity.CognitoUser = {} as any;
  private _storage: cognito_storage;

  constructor(private _context: ExtensionContext) {
    this._user_pool = new AmazonCognitoIdentity.CognitoUserPool(this._pool_data);
    this._storage = new cognito_storage(_context);

    console.log((workspace.getConfiguration().get('ArtOfCode.servers')));
  }

  refresh_servers() {
    apiBaseUrl = `${get_server_uri().server}/`;
    this._user_pool = new AmazonCognitoIdentity.CognitoUserPool(this._pool_data);
  }

  private _parse_check_response(report: any): JSErrorSign[] {

    return _.chain(report)
      .transform((acc: any[], values: any[], _key: string) => {
        acc.push(...values);
        return acc;
      }, [])
      .map((error: any) => ({
        title: error.code,
        type: error.info ? "INFO" : "ERROR",
        message: error.msg,
        link: 'https://artofcode.eu',
        start: {
          line: error.loc.start.line - 1,
          column: error.loc.start.column,
        },
        end: {
          line: error.loc.end.line - 1,
          column: error.loc.end.column,
        },
      }))
        .value();
  }

  public create_cancel_token() {
    return { abort: () => {} };
  }

  runCheck = async (filepath: string, buf: Buffer, abort_token: ReturnType<api["create_cancel_token"]>) => {
    let   abort = false;
    const filename = path.basename(filepath);
    const body = new FormData();

    body.append('file', buf);

    abort_token.abort = () => abort = true;

    try {
      const request_result: any = await new Promise((resolve, reject) => {
        const url = new URL(apiBaseUrl + 'backoffice/process');

        const user_tokens = this._cognito_user.getSignInUserSession();
        if (user_tokens === null || !!abort) { return; }

        const client = body.submit({
          protocol: url.protocol as 'http:' | 'https:',
          host: url.hostname,
          path: url.pathname,
          port: url.port,
          headers: { 'Authorization': `Bearer ${ user_tokens.getIdToken().getJwtToken() }` }
        }, (err: any, resp: any) => {
          if (err) { reject(err); return; }
          
          let data = '';

          resp.on('data', (chunk: any) => {
            data += chunk;
          });

          resp.on('end', () => {
            resolve(JSON.parse(data));
          });

        });

        abort_token.abort = () => { abort = true; client.abort(); };
      });
  
      const errors = request_result.errors;

      if (errors.statusCode !== 200) {
        errors.body = typeof errors.infos === 'object'
                          ? errors.infos
                          : JSON.parse(errors.infos);

        await Promise.reject({
          source: errors.headers,
          infos: errors.infos,
          message: errors.infos.errorMessage 
        });
      }

      return this._parse_check_response(errors.body);
    } catch (e) {
      if (!abort) {
        console.error(e);
        if (!!e.infos) {
          // let err = new Error();
          // err.message = e.infos.errorMessage;
          // err.stack = e.infos.trace;
          // err.name = e.inofs.ruleName;
          // console.error(err);
          if (!!e.infos.trace) {
            console.error(e.infos.trace.join('\n'));
          }
        }
        throw e;
      }
    }
  };

  public async logout() {
    const user = await this.get_session();

    if (!!user) {
      return new Promise((resolve, reject) => {
        user.globalSignOut({
          onSuccess: resolve,
          onFailure: reject
        });
      });
    }
  }

  public async login(username: string, password: string): Promise<any> {
    const authentication_details = new AmazonCognitoIdentity.AuthenticationDetails({
      Username : username,
      Password : password,
    });
    const user_data = {
      Username: username,
      Pool: this._user_pool,
      Storage: this._storage,
    };
    this._cognito_user = new AmazonCognitoIdentity.CognitoUser(user_data);
    
    return new Promise<AmazonCognitoIdentity.CognitoUserSession>((resolve, reject) => {
      this._cognito_user.authenticateUser(authentication_details, {
        onSuccess: resolve,
        onFailure: reject
      });
    }).then(r => {
      let res = {
        access_token: r.getAccessToken().getJwtToken(),
        id_token: r.getIdToken().getJwtToken(),
        refresh_token: r.getRefreshToken().getToken(),
        attributes: {} as any
      };

      return new Promise((resolve, reject) => {
        this._cognito_user.getUserAttributes((err, result: any) => {
          if (err) {
            console.error(err.message || JSON.stringify(err));
            reject(err);
            return;
          }
          for (let i = 0; i < result.length; i++) {
            res.attributes[result[i].getName()] = result[i].getValue();
          }
          resolve(res);
        });
      });
    });
  }

  lastTimeout: any;

  public async find_doc(keyword: string) {
    let cache = new Map<string, any[]>();
    let v;

    if (!!this.lastTimeout) {
      clearTimeout(this.lastTimeout);
    }
    /// clear cache every hours
    this.lastTimeout = setTimeout(() => {
      cache = new Map<string, any[]>();
      this.lastTimeout = null;
    }, 1000 * 60 * 60);

    if (!!(v = cache.get("keyword"))) {
      return v;
    }

    return axios.get(apiBaseUrl + 'documentation/find/' + encodeURI(keyword))
      .then(r => (r.data))
      .then(r => {
        cache.set(keyword, r);
        return r;
      });
  } 

  private _cognito_get_current_user() {
    const last_user_key = `CognitoIdentityServiceProvider.${
      this._user_pool.getClientId()
    }.LastAuthUser`;

    const last_auth_user = this._storage.getItem(last_user_key);
    if (last_auth_user) {
      const cognito_user = {
        Username: last_auth_user,
        Pool: this._user_pool,
        Storage: this._storage,
      };

      return new AmazonCognitoIdentity.CognitoUser(cognito_user);
    }

    return null;
  }

  public async get_session() {
    return new Promise<AmazonCognitoIdentity.CognitoUser>((resolve, reject) => {
      const user = this._cognito_get_current_user();
  
      if (!!user) {
        this._cognito_user = user;
  
        user.getSession((err: any, result: any) => {
          if (err) {
            reject(err);
          }
          resolve(user);
        });
      } else {
        reject('never connected'); 
      }
    });
  }

}

export default api;
