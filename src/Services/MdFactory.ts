/* eslint-disable @typescript-eslint/class-name-casing */
import { MarkdownString } from "vscode";

interface js_error {
  title: string,
  message: string,
  link?: string
}


class md_factory {
  static makeJsError(options: js_error) {
    let lines = [
      `**${options.title}**  `,
      '\n---\n',
      `${options.message}  `,
      (!!options.link) ? `[Help me](${options.link} "Open a documentation that can help you")` : null
    ];

    let mk = new MarkdownString();
    lines.forEach((line) => {
      if (line === null) { return; }
      mk.appendMarkdown(line + '\n');
    });
    mk.isTrusted = true;
    return mk;
  }
}

export {
  md_factory,
  js_error,
};
