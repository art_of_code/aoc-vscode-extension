'use strict';

import * as vscode from 'vscode';
import aoc_api from "./Services/api";
import { login } from './Commands/login';
import { single_file_check } from './Commands/single-file-check';
import { logout } from './Commands/logout';
(<any>global).fetch = require('node-fetch').default;

const register_command: (command?: any, callback?: any, thisArg?: any) => any = vscode.commands.registerCommand;

export function activate(context: vscode.ExtensionContext) {

  const api = new aoc_api(context);

  const changesub = vscode.workspace.onDidChangeConfiguration(async event => {
    let affected = event.affectsConfiguration("ArtOfCode.servers");
    if (affected) {
      await vscode.commands.executeCommand('artofcode.logout');
      api.refresh_servers();
    }
  });

  context.subscriptions.push(changesub);
  context.subscriptions.push(register_command(...new login(context, api).get_args()));
  context.subscriptions.push(register_command(...new logout(api).get_args()));
  context.subscriptions.push(register_command(...new single_file_check(api).get_args()));
}

export function deactivate() {

}
