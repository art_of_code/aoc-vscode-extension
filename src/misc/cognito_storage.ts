/* eslint-disable @typescript-eslint/class-name-casing */
import { ExtensionContext } from "vscode";

class cognito_storage {
  constructor (private _context: ExtensionContext) {}

  private _get_storage(): { [key: string]: string } { return this._context.globalState.get('internal_cognito_storage') || {}; }
  private _update_storage(storage: any) { this._context.globalState.update('internal_cognito_storage', storage).then(() => {}); }

  setItem(key: string, value: string): string {
    let storage = this._get_storage();
    storage[key] = value;
    this._update_storage(storage);
    return value;
  }

  getItem(key: string): string {
    return this._get_storage()[key];
  }

  removeItem(key: string): string {
    let   storage = this._get_storage();
    const value = storage[key];
    delete storage[key];
    this._update_storage(storage);
    return value;
  }

  clear(): {} {
    this._update_storage({});
    return {};
  }
}

export default cognito_storage;